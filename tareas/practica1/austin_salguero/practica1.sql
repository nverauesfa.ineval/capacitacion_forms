
-- Create table
create table INFO_CLIENTES
(
  id_cliente number not null,
  nombre     varchar2(250) not null,
  direccion  varchar2(500),
  telefono   varchar2(15) not null
)
;
-- Add comments to the table 
comment on table INFO_CLIENTES
  is 'tabla de informacion de los clientes';
-- Add comments to the columns 
comment on column INFO_CLIENTES.id_cliente
  is 'identificador unico de la tabla INFO_CLIENTES';
comment on column INFO_CLIENTES.nombre
  is 'nombre de los clientes';
comment on column INFO_CLIENTES.direccion
  is 'direccion de los clientes';
comment on column INFO_CLIENTES.telefono
  is 'telefono de los clientes';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CLIENTES
  add constraint pk_clientes_id primary key (ID_CLIENTE);
--------------------------------------------------------

-- Create table
create table INFO_FACTURAS
(
  id_factura number not null,
  fecha      date not null,
  cliente_id number not null
)
;
-- Add comments to the table 
comment on table INFO_FACTURAS
  is 'tabla que contiene informacion de las facturas';
-- Add comments to the columns 
comment on column INFO_FACTURAS.id_factura
  is 'identificador unico de la tabla INFO_FACTURAS';
comment on column INFO_FACTURAS.fecha
  is 'fecha de las facturas';
comment on column INFO_FACTURAS.cliente_id
  is 'identificador que hace referencia a la tabla INFO_CLIENTES a la columna id_cliente';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_FACTURAS
  add constraint pk_facturas_id primary key (ID_FACTURA);
alter table INFO_FACTURAS
  add constraint fk_client_clientes_id foreign key (CLIENTE_ID)
  references info_clientes (ID_CLIENTE);
-------------------------------------------------------

-- Create table
create table INFO_PROVEEDORES
(
  id_proveedor number not null,
  nombre       varchar2(250) not null,
  direccion    varchar2(1000) not null,
  telefono     varchar2(15) not null
)
;
-- Add comments to the table 
comment on table INFO_PROVEEDORES
  is 'tabla que contiene informacion de los proveedores';
-- Add comments to the columns 
comment on column INFO_PROVEEDORES.id_proveedor
  is 'identificador unico de la tabla INFO_PROVEEDOR';
comment on column INFO_PROVEEDORES.nombre
  is 'nombre de los proveedores';
comment on column INFO_PROVEEDORES.direccion
  is 'direccion de los proveedores';
comment on column INFO_PROVEEDORES.telefono
  is 'telefono de los proveedores';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PROVEEDORES
  add constraint pk_proveedor_id primary key (ID_PROVEEDOR);
---------------------------------------------------------
-- Create table
create table INFO_CATEGORIAS
(
  id_categoria number not null,
  descripcion  varchar2(250) not null
)
;
-- Add comments to the table 
comment on table INFO_CATEGORIAS
  is 'tabla que contiene informacion de las categorias';
-- Add comments to the columns 
comment on column INFO_CATEGORIAS.id_categoria
  is 'identificador unico de la tabla INFO_CATEGORIAS';
comment on column INFO_CATEGORIAS.descripcion
  is 'descripcion de las categorias';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CATEGORIAS
  add constraint pk_categoria_id primary key (ID_CATEGORIA);
----------------------------------------------
-- Create table
create table INFO_PRODUCTOS
(
  id_producto  number not null,
  descripcion  varchar2(1000) not null,
  precio       number default 0.0 not null,
  categoria_id number not null,
  proveedor_id number not null
)
;
-- Add comments to the table 
comment on table INFO_PRODUCTOS
  is 'tabla que contiene informacion de los productos';
-- Add comments to the columns 
comment on column INFO_PRODUCTOS.id_producto
  is 'identificador unico de la tabla INFO_PRODUCTOS';
comment on column INFO_PRODUCTOS.descripcion
  is 'descripcion de los productos';
comment on column INFO_PRODUCTOS.precio
  is 'precio de los productos';
comment on column INFO_PRODUCTOS.categoria_id
  is 'identificador que hace referencia a la table INFO_CATEGORIAS a la columna id_categoria';
comment on column INFO_PRODUCTOS.proveedor_id
  is 'identificador que referencia a la tabla INFO_PROVEEDORES a la columna id_proveedor';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PRODUCTOS
  add constraint pk_producto_id primary key (ID_PRODUCTO);
alter table INFO_PRODUCTOS
  add constraint fk_cat_categoria_id foreign key (CATEGORIA_ID)
  references info_categorias (ID_CATEGORIA);
alter table INFO_PRODUCTOS
  add constraint fk_prov_proveedor_id foreign key (PROVEEDOR_ID)
  references info_proveedores (ID_PROVEEDOR);
--------------------------------------------------------

-- Create table
create table INFO_VENTAS
(
  id_venta    number not null,
  factura_id  number not null,
  producto_id number not null,
  cantidad    number default 0 not null
)
;
-- Add comments to the table 
comment on table INFO_VENTAS
  is 'tabla que contiene informacion sobre las ventas';
-- Add comments to the columns 
comment on column INFO_VENTAS.id_venta
  is 'identificador unico de la tabla INFO_VENTAS';
comment on column INFO_VENTAS.factura_id
  is 'identificador que hace referencia a la tabla INFO_FACTURAS a la columna id_factura';
comment on column INFO_VENTAS.producto_id
  is 'identificador que hace refenrencia a la tabla INFO_PRODUCTOS a la columna id_producto';
comment on column INFO_VENTAS.cantidad
  is 'cantidad del producto en venta';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_VENTAS
  add constraint pk_venta_id primary key (ID_VENTA);
alter table INFO_VENTAS
  add constraint fk_fact_factura_id foreign key (FACTURA_ID)
  references info_facturas (ID_FACTURA);
alter table INFO_VENTAS
  add constraint fk_prod_producto_id foreign key (PRODUCTO_ID)
  references info_productos (ID_PRODUCTO);

